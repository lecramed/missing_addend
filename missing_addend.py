from random import randint

def main():
    def equation(guess, max_sum):
        x = randint(0,max_sum)
        y = guess
        z = randint(0,max_sum)
        #if randint(0,1) == 0 and x > z:
        if x > z:
            a = '-'
        else:
            a = '+'
        return [x, a, y, z]

    biggest_number = input('\nType the largest number you would like to test to: ')
    try:
        biggest_number = int(biggest_number)
    except:
        print('\n  Numbers only please! Defaulted to 18.')
        biggest_number = 18

    play_again = True
    while play_again == True:
        values = equation('?', biggest_number)
        print('\n{} {} {} = {}'.format(values[0], values[1], values[2], values[3]))
        guess = input('Type the missing number: ')
        try:
            if values[1] == '+':
                answer = values[0] + int(guess)
            elif values[1] == '-':
                answer = values[0] - int(guess)
            if answer == values[3]:
                print('\n     Correct!  : )')
            else:
                print('\n     Incorrect. : (\n     It\'s okay, try again!')
        except:
            print('\n  Numbers only please!')
        again = input('\nPress enter to play again, or type "q" to quit: ')
        if again == 'q':
            play_again = False
    print('\nType: "python missing_addend.py" to play again.')

if __name__ == '__main__':
    main()
